package MainPackage;

/**
 *
 * @author goda
 */
class Listener2 extends Observer{
 
 public Listener2(Subject subject){
       this.subject = subject;
       this.subject.append(this);
 }

    @Override
    public void update() {
         System.out.println("Here is listener2 "+this.subject.getCount());
    }

}

