package MainPackage;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author goda
 */
class Subject{
int count;
   List <Observer> observers = new ArrayList<>();
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
        this.NotifyAll();
    }
    
    public void append(Observer observer){
        observers.add(observer);
    }
    
    public void NotifyAll(){
        observers.forEach((observer) -> {
            observer.update();
    });
    }

}