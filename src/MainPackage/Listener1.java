package MainPackage;

/**
 *
 * @author goda
 */
 class Listener1 extends Observer{
 
 public Listener1(Subject subject){
       this.subject = subject;
       this.subject.append(this);
 }

    @Override
    public void update() {
         System.out.println("Here is listener1 "+this.subject.getCount());
    }

} 

