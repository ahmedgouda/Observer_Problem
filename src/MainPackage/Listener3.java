package MainPackage;

/**
 *
 * @author goda
 */
class Listener3 extends Observer{
 
 public Listener3(Subject subject){
       this.subject = subject;
       this.subject.append(this);
 }

    @Override
    public void update() {
         System.out.println("Here is listener3 "+this.subject.getCount());
    }

}