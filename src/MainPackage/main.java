package MainPackage;

public class main{

    public static void main(String[] args) {
        Subject subject = new Subject();
       Listener1 listener1 = new Listener1(subject);
       Listener2 listener2 = new Listener2(subject);
       Listener3 listener3 = new Listener3(subject);
        
       subject.setCount(5);
       
    }
}