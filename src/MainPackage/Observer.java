package MainPackage;

/**
 *
 * @author goda
 */
abstract class Observer{
 protected  Subject subject;
 public abstract void update();
}